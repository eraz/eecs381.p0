#include "meeting.h"


#include "schedule.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>

Meeting::Meeting(int time, std::string const& topic)
: m_time{time}, m_topic{topic} {
	// Case: time out of range.
	if (!in_range(time)) {
		throw Error{"Time is not in range!"};
	}
}

Meeting::Meeting(std::ifstream& ifs, people_list_t people_list) {
	read_int(ifs, m_time);

	// Case: premature end of file.
	if (!(ifs >> m_topic)) throw Error{""};

	int participant_size; read_int(ifs, participant_size);

	// Case: numeric value for the number of items to be read is negative.
	if (participant_size < 0) throw Error{""};

	participant_list.reserve(participant_size);

	for (int i = 0; i < participant_size; ++i) {
		std::string lastname; ifs >> lastname;

		// Case: premature end of file.
		if (!ifs) throw Error{""};

		// Case: participant name not found in the person list.
		if (std::find(
				std::begin(people_list),
				std::end(people_list),
				lastname) == std::end(people_list)) {
			throw Error{""};
		}

		participant_list.emplace_back(lastname);
	}

	// No requirement to check for sorted participant list during load.
//assert(std::is_sorted(std::begin(participant_list), std::end(participant_list)));
}

bool Meeting::in_range(int time) {
	return (1 <= time && time <= 5) || (9 <= time && time <= 12);
}

bool Meeting::is_participant(std::string const& lastname) const {
	auto const& list = participant_list;
	return std::find(std::begin(list), std::end(list), lastname) != std::end(list);
}

std::ostream& Meeting::print(std::ostream& os, people_list_t people_list) const {
	os << "Meeting time: " << m_time
			<< ", Topic: " << m_topic
			<< "\nParticipants:";

	auto const& list = participant_list;

	assert(std::is_sorted(std::begin(list), std::end(list)));

	if (list.empty()) os << " None" << '\n';
	else {
		os << '\n';

		assert(std::is_sorted(std::begin(list), std::end(list)));

		for (std::string const& lastname : participant_list) {
			// Find person with lastname.
			auto iter = std::find(std::begin(people_list), std::end(people_list), lastname);

			// Participant should be in person list.
			assert(iter != std::end(people_list));

			iter->print(os);
		}
	}

	return os;
}

void Meeting::add_participant(people_list_t people_list) {
	std::string lastname; std::cin >> lastname;

	// Case: no person in the person list of that name.
	if (std::find(std::begin(people_list), std::end(people_list), lastname) == std::end(people_list)) {
		throw Error{"No person with that name!"};
	}

	auto& list = participant_list;

	// Find position for new participant.
	auto iter = std::lower_bound(std::begin(list), std::end(list), lastname);

	// Case: already a participant of that name.
	if (iter != std::end(list) && *iter == lastname) {
		throw Error{"This person is already a participant!"};
	}

	list.insert(iter, lastname);

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Participant " << lastname << " added" << '\n';
}

void Meeting::reschedule(int time) {
	m_time = time;
}

void Meeting::del_participant(people_list_t people_list) {
	std::string lastname; std::cin >> lastname;

	// Case: no person in the person list of that name.
	if (std::find(std::begin(people_list), std::end(people_list), lastname) == std::end(people_list)) {
		throw Error{"No person with that name!"};
	}

	auto& list = participant_list;

	// Find participant with lastname.
	auto iter = std::find(std::begin(list), std::end(list), lastname);

	// Case: no person of that name in the participant list.
	if (iter == std::end(list)) throw Error{"This person is not a participant in the meeting!"};

	list.erase(iter);

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Participant " << lastname << " deleted" << '\n';
}

void Meeting::save(std::ofstream& ofs) const {
	auto const& list = participant_list;

	ofs << m_time << " " << m_topic << " " << list.size() << '\n';
	for (std::string const& lastname : list) ofs << lastname << '\n';
}

bool operator<(Meeting const& a, Meeting const& b) {
	assert(Meeting::in_range(a.m_time));
	assert(Meeting::in_range(b.m_time));

	// Case: AM < PM.
	if (a.m_time <= 5 && b.m_time >= 9) return false;

	// Case: PM > AM.
	if (a.m_time >= 9 && b.m_time <= 5) return true;

	return a.m_time < b.m_time;
}

bool operator==(Meeting const& a, Meeting const& b) {
	return !(a < b) && !(b < a);
}
