#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

class Person {
	std::string m_lastname;
	std::string m_firstname;
	std::string m_phone_number;

public:
	Person(
			std::string const& lastname,
			std::string const& firstname = std::string(),
			std::string const& phone_number = std::string());

	std::string const& lastname() const { return m_lastname; }

	std::ostream& print(std::ostream& os) const;

	friend bool operator<(Person const& a, Person const& b);
	friend bool operator==(Person const& a, Person const& b);
};

#endif
