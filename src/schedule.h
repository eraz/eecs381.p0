#ifndef SCHEDULE_H
#define SCHEDULE_H

#include "person.h"
#include "room.h"

#include <iostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

struct Error : std::runtime_error {
	explicit Error(std::string const& what_arg) : std::runtime_error(what_arg) {}
	explicit Error(char const* what_arg) : std::runtime_error(what_arg) {}
};

class Schedule {
	std::vector<Person> people_list;
	std::vector<Room>   room_list;

public:
	Person const& person(std::string const& lastname) const;
	Room const& room(int number) const;

	std::ostream& print(std::ostream& os) const;
	std::ostream& print_person(std::ostream& os) const;
	std::ostream& print_room(std::ostream& os) const;
	std::ostream& print_meeting(std::ostream& os) const;
	std::ostream& print_group(std::ostream& os) const;
	std::ostream& print_allocations(std::ostream& os) const;

	void add_person();
	void add_room();
	void add_meeting();
	void add_participant();

	void reschedule();

	void del();
	void del_person();
	void del_room();
	void del_meeting();
	void del_participant();
	void del_group();
	void del_all();

	void save() const;
	void load();

private:
	Room& get_room(int number);
};

std::istream& read_int(std::istream& is, int& value);

#endif
