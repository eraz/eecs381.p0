#ifndef ROOM_H
#define ROOM_H

#include "meeting.h"

#include <iostream>
#include <vector>

class Person;

class Room {
	using people_list_t = std::vector<Person> const&;

	int m_number;
	std::vector<Meeting> meeting_list;

public:
	Room(int number);
	Room(std::ifstream& ifs, people_list_t people_list);

	auto num_meetings() const { return meeting_list.size(); }
	Meeting const& meeting(int time) const;
	bool is_participant(std::string const& lastname) const;

	std::ostream& print(std::ostream& os, people_list_t people_list) const;

	void add_meeting();
	void add_participant(people_list_t people_list);

	void reschedule(int time, Room& new_room, int new_time);

	void del();
	void del_meeting();
	void del_participant(people_list_t people_list);

	void save(std::ofstream& ofs) const;

	friend bool operator<(Room const& a, Room const& b);
	friend bool operator==(Room const& a, Room const& b);

private:
	Meeting& get_meeting(int time);
};

#endif
