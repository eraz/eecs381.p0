#include "schedule.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <numeric>
#include <string>

Person const& Schedule::person(std::string const& lastname) const {
	auto const& list = people_list;

	// Find person with last name.
	auto iter = std::find(std::begin(list), std::end(list), lastname);

	// Case: no person with that last name.
	if (iter == std::end(list)) throw Error{"No person with that name!"};

	return *iter;
}

Room const& Schedule::room(int number) const {
	auto const& list = room_list;

	// Find room with number.
	auto iter = std::find(std::begin(list), std::end(list), number);

	// Case: no room of that number.
	if (iter == std::end(list)) throw Error{"No room with that number!"};

	return *iter;
}

std::ostream& Schedule::print(std::ostream& os) const {
	auto const& list = room_list;

	if (list.empty()) os << "List of rooms is empty" << '\n';
	else {
		os << "Information for " << list.size() << " rooms:" << '\n';
		for (Room const& room : list) room.print(os, people_list);
	}

	return os;
}

std::ostream& Schedule::print_person(std::ostream& os) const {
	std::string lastname; std::cin >> lastname;
	return person(lastname).print(os);
}

std::ostream& Schedule::print_room(std::ostream& os) const {
	int number; read_int(std::cin, number);
	return room(number).print(os, people_list);
}

std::ostream& Schedule::print_meeting(std::ostream& os) const {
	int number; read_int(std::cin, number);
	int time; read_int(std::cin, time);
	return room(number).meeting(time).print(os, people_list);
}

std::ostream& Schedule::print_group(std::ostream& os) const {
	auto const& list = people_list;

	if (list.empty()) os << "List of people is empty" << '\n';
	else {
		os << "Information for " << list.size() << " people:" << '\n';
		for (Person const& person : people_list) person.print(os);
	}

	return os;
}

std::ostream& Schedule::print_allocations(std::ostream& os) const {
	auto const& list = room_list;

	auto num_meetings = std::accumulate(
			std::begin(list),
			std::end(list),
			std::size_t{0},
			[](std::size_t sum, Room const& room) {
				return sum + room.num_meetings();
			});

	return os << "Memory allocations:" << '\n'
			<< "Persons: " << people_list.size() << '\n'
			<< "Meetings: " << num_meetings << '\n'
			<< "Rooms: " << room_list.size() << '\n';
}

void Schedule::add_person() {
	std::string firstname, lastname, phone_number;
	std::cin >> firstname >> lastname >> phone_number;

	Person person{lastname, firstname, phone_number};

	auto& list = people_list;

	// Find position for new person.
	auto iter = std::lower_bound(std::begin(list), std::end(list), person);

	// Case: person with that last name is already in the people list.
	if (iter != std::end(list) && *iter == person) {
		throw Error{"There is already a person with this last name!"};
	}

	list.insert(iter, std::move(person));

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Person " << lastname << " added" << '\n';
}

void Schedule::add_room() {
	int number; read_int(std::cin, number);

	Room room{number};

	auto& list = room_list;

	// Find position for new room.
	auto iter = std::lower_bound(std::begin(list), std::end(list), room);

	// Case: room of that number already exists.
	if (iter != std::end(list) && *iter == room) {
		throw Error{"There is already a room with this number!"};
	}

	list.insert(iter, std::move(room));

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Room " << number << " added" << '\n';
}

void Schedule::add_meeting() {
	int number; read_int(std::cin, number);
	get_room(number).add_meeting();
}

void Schedule::add_participant() {
	int number; read_int(std::cin, number);
	get_room(number).add_participant(people_list);
}

void Schedule::reschedule() {
	int old_number; read_int(std::cin, old_number);
	Room& old_room = get_room(old_number);

	int old_time; read_int(std::cin, old_time);
	{ // Check that meeting exists.
		Meeting const& old_meeting = old_room.meeting(old_time);
		(void)old_meeting;
	}

	int new_number; read_int(std::cin, new_number);
	Room& new_room = get_room(new_number);

	int new_time; read_int(std::cin, new_time);

	// Case: time out of range.
	if (!Meeting::in_range(new_time)) throw Error{"Time is not in range!"};

	old_room.reschedule(old_time, new_room, new_time);
}

void Schedule::del() {
	for (Room& room : room_list) room.del();
	std::cout << "All meetings deleted" << '\n';
}

void Schedule::del_person() {
	std::string lastname; std::cin >> lastname;

	auto& list = people_list;

	// Find person with lastname.
	auto iter = std::find(std::begin(list), std::end(list), lastname);

	// Case: no person of that name.
	if (iter == std::end(list)) throw Error{"No person with that name!"};

	// Check if person is a participant in a meeting.
	for (Room const& room : room_list) if (room.is_participant(lastname)) {
		throw Error{"This person is a participant in a meeting!"};
	}

	list.erase(iter);

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Person " << lastname << " deleted" << '\n';
}

void Schedule::del_room() {
	int number; read_int(std::cin, number);

	auto& list = room_list;

	// Find room with number.
	auto iter = std::find(std::begin(list), std::end(list), number);

	// Case: no room of that number.
	if (iter == std::end(list)) throw Error{"No room with that number!"};

	list.erase(iter);

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Room " << number << " deleted" << '\n';
}

void Schedule::del_meeting() {
	int number; read_int(std::cin, number);
	get_room(number).del_meeting();
}

void Schedule::del_participant() {
	int number; read_int(std::cin, number);
	get_room(number).del_participant(people_list);
}

void Schedule::del_group() {
	// Case: a meeting is scheduled.
	for (Room const& room : room_list) if (room.num_meetings()) {
		throw Error{"Cannot clear people list unless there are no meetings!"};
	}

	people_list.clear();

	std::cout << "All persons deleted" << '\n';
}

void Schedule::del_all() {
	room_list.clear();
	people_list.clear();

	std::cout << "All rooms and meetings deleted" << '\n'
			<< "All persons deleted" << '\n';
}

void Schedule::save() const {
	std::string filename; std::cin >> filename;
	std::ofstream ofs{filename};

	// Case: file cannot be opened for output.
	if (!ofs.is_open()) throw Error{"Could not open file!"};

	ofs << people_list.size() << '\n';
	for (Person const& person : people_list) person.print(ofs);

	ofs << room_list.size() << '\n';
	for (Room const& room : room_list) room.save(ofs);

	std::cout << "Data saved" << '\n';
}

void Schedule::load() {
	std::string filename; std::cin >> filename;
	std::ifstream ifs{filename};

	// Case: file cannot be opened for input.
	if (!ifs.is_open()) throw Error{"Could not open file!"};

	try {
		room_list.clear();
		people_list.clear();

		int people_size; read_int(ifs, people_size);

		// Case: numeric value for the number of items to be read is negative.
		if (people_size < 0) throw Error{""};

		people_list.reserve(people_size);

		for (int i = 0; i < people_size; ++i) {
			std::string firstname, lastname, phone_number;

			// Case: premature end of file.
			if (!(ifs >> firstname >> lastname >> phone_number)) throw Error{""};

			people_list.emplace_back(lastname, firstname, phone_number);
		}

		int room_size; read_int(ifs, room_size);

		// Case: numeric value for the number of items to be read is negative.
		if (people_size < 0) throw Error{""};

		room_list.reserve(room_size);

		for (int i = 0; i < room_size; ++i) room_list.emplace_back(ifs, people_list);

	} catch (Error const& e) {
		room_list.clear();
		people_list.clear();
		throw Error{"Invalid data found in file!"};
	}

	// No requirement to check for sorted people/room list during load.
//assert(std::is_sorted(std::begin(people_list), std::end(people_list)));
//assert(std::is_sorted(std::begin(room_list), std::end(room_list)));

	std::cout << "Data loaded" << '\n';
}

Room& Schedule::get_room(int number) {
	auto& list = room_list;

	// Find room with number.
	auto iter = std::find(std::begin(list), std::end(list), number);

	// Case: no room of that number.
	if (iter == std::end(list)) throw Error{"No room with that number!"};

	return *iter;
}

std::istream& read_int(std::istream& is, int& value) {
	// Case: failed to read integer.
	if (!(is >> value)) throw Error{"Could not read an integer value!"};

	return is;
}
