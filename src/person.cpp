#include "person.h"

#include <iostream>
#include <string>

Person::Person(
		std::string const& lastname,
		std::string const& firstname,
		std::string const& phone_number)
: m_lastname{lastname}, m_firstname{firstname}, m_phone_number{phone_number} {}

std::ostream& Person::print(std::ostream& os) const {
	return os << m_firstname << " "
			<< m_lastname << " "
			<< m_phone_number << '\n';
}

bool operator<(Person const& a, Person const& b) {
	return a.m_lastname < b.m_lastname;
}

bool operator==(Person const& a, Person const& b) {
	return !(a < b) && !(b < a);
}
