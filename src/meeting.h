#ifndef MEETING_H
#define MEETING_H

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

class Person;

class Meeting {
	using people_list_t = std::vector<Person> const&;

	int m_time;
	std::string m_topic;
	std::vector<std::string> participant_list;

public:
	Meeting(int time, std::string const& topic = std::string());
	Meeting(std::ifstream& ifs, people_list_t people_list);

	static bool in_range(int time);
	bool is_participant(std::string const& lastname) const;

	std::ostream& print(std::ostream& os, people_list_t people_list) const;

	void add_participant(people_list_t people_list);

	void reschedule(int time);

	void del_participant(people_list_t people_list);

	void save(std::ofstream& ofs) const;

	friend bool operator<(Meeting const& a, Meeting const& b);
	friend bool operator==(Meeting const& a, Meeting const& b);
};

#endif
