#include "room.h"

#include "meeting.h"
#include "schedule.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>

Room::Room(int number) : m_number{number} {
	// Case: room number out of range.
	if (number <= 0) throw Error{"Room number is not in range!"};
}

Room::Room(std::ifstream& ifs, people_list_t people_list) {
	read_int(ifs, m_number);

	int meeting_size; read_int(ifs, meeting_size);

	// Case: numeric value for the number of items to be read is negative.
	if (meeting_size < 0) throw Error{""};

	meeting_list.reserve(meeting_size);

	for (int i = 0; i < meeting_size; ++i) meeting_list.emplace_back(ifs, people_list);

	// No requirement to check for sorted meeting list during load.
//assert(std::is_sorted(std::begin(meeting_list), std::end(meeting_list)));
}

Meeting const& Room::meeting(int time) const {
	auto const& list = meeting_list;

	// Find meeting at time.
	auto iter = std::find(std::begin(list), std::end(list), time);

	// Case: no meeting at that time.
	if (iter == std::end(list)) throw Error{"No meeting at that time!"};

	return *iter;
}

bool Room::is_participant(std::string const& lastname) const {
	for (Meeting const& meeting : meeting_list) {
		if (meeting.is_participant(lastname)) return true;
	}

	return false;
}

std::ostream& Room::print(std::ostream& os, people_list_t people_list) const {
	os << "--- Room " << m_number << " ---" << '\n';

	auto const& list = meeting_list;

	if (list.empty()) os << "No meetings are scheduled" << '\n';
	else for (Meeting const& meeting : list) meeting.print(os, people_list);

	return os;
}

void Room::add_meeting() {
	int time; read_int(std::cin, time);
	Meeting::in_range(time);

	std::string topic; std::cin >> topic;
	Meeting meeting{time, topic};

	auto& list = meeting_list;

	// Find position for new meeting.
	auto iter = std::lower_bound(std::begin(list), std::end(list), meeting);

	// Case: meeting at that time already exists in this room.
	if (iter != std::end(list) && *iter == meeting) {
		throw Error{"There is already a meeting at that time!"};
	}

	list.insert(iter, std::move(meeting));

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Meeting added at " << time << '\n';
}

void Room::add_participant(people_list_t people_list) {
	int time; read_int(std::cin, time);
	Meeting::in_range(time);
	get_meeting(time).add_participant(people_list);
}

void Room::reschedule(int time, Room& new_room, int new_time) {
	// Find meeting to reschedule.
	auto iter = std::find(std::begin(meeting_list), std::end(meeting_list), time);

	// Case: meeting does not exist at time.
	if (iter == std::end(meeting_list)) throw Error{"No meeting at that time!"};

	// Case: a meeting at the new time already exists in the new room.
	if (*this == new_room && time == new_time) {
		throw Error{"There is already a meeting at that time!"};
	}

	Meeting meeting{std::move(*iter)};
	meeting_list.erase(iter);

	meeting.reschedule(new_time);

	// Find new position for meeting.
	auto new_iter = std::lower_bound(
			std::begin(new_room.meeting_list),
			std::end(new_room.meeting_list),
			new_time);

	if (new_iter != std::end(new_room.meeting_list) && *new_iter == new_time) {
		*new_iter = std::move(meeting);
	} else {
		new_room.meeting_list.insert(new_iter, std::move(meeting));
	}

	assert(std::is_sorted(std::begin(meeting_list), std::end(meeting_list)));
	assert(std::is_sorted(std::begin(new_room.meeting_list), std::end(new_room.meeting_list)));

	std::cout << "Meeting rescheduled to room " << new_room.m_number << " at " << new_time << '\n';
}

void Room::del() {
	meeting_list.clear();
}

void Room::del_meeting() {
	int time; read_int(std::cin, time);

	auto& list = meeting_list;

	// Find meeting at time.
	auto iter = std::find(std::begin(list), std::end(list), time);

	// Case: no meeting at that time.
	if (iter == std::end(list)) throw Error{"No meeting at that time!"};

	list.erase(iter);

	assert(std::is_sorted(std::begin(list), std::end(list)));

	std::cout << "Meeting at " << time << " deleted" << '\n';
}

void Room::del_participant(people_list_t people_list) {
	int time; read_int(std::cin, time);
	get_meeting(time).del_participant(people_list);
}

Meeting& Room::get_meeting(int time) {
	auto& list = meeting_list;

	// Find meeting at time.
	auto iter = std::find(std::begin(list), std::end(list), time);

	// Case: no meeting at that time.
	if (iter == std::end(list)) throw Error{"No meeting at that time!"};

	assert(std::is_sorted(std::begin(list), std::end(list)));

	return *iter;
}

void Room::save(std::ofstream& ofs) const {
	auto const& list = meeting_list;

	ofs << m_number << " " << list.size() << '\n';
	for (Meeting const& meeting : list) meeting.save(ofs);
}

bool operator<(Room const& a, Room const& b) {
	return a.m_number < b.m_number;
}

bool operator==(Room const& a, Room const& b) {
	return !(a < b) && !(b < a);
}
