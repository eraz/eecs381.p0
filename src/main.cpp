#include "schedule.h"
#include "strings.h"

#include <exception>
#include <iostream>
#include <limits>

namespace Command {
	enum Action : char {
		Print      = 'p',
		Add        = 'a',
		Reschedule = 'r',
		Del        = 'd',
		Save       = 's',
		Load       = 'l'
	};

	enum Object : char {
		Person      = 'i',
		Room        = 'r',
		Meeting     = 'm',
		Participant = 'p',
		Schedule    = 's',
		Group       = 'g',
		All         = 'a',
		Data        = 'd'
	};
}

std::istream& operator>>(std::istream& is, Command::Action& action);
std::istream& operator>>(std::istream& is, Command::Object& object);

int main() try {
	Schedule schedule;

	while (true) {
		std::cout << "\nEnter command: ";

		Command::Action action; std::cin >> action;
		Command::Object object; std::cin >> object;

		// Case: command to terminate the program.
		if (action == 'q' && object == 'q') {
			schedule.del_all();
			break;
		}

		try {
			switch (action) {
			case Command::Print : switch (object) {
				case Command::Person   : schedule.print_person(std::cout);      break;
				case Command::Room     : schedule.print_room(std::cout);        break;
				case Command::Meeting  : schedule.print_meeting(std::cout);     break;
				case Command::Schedule : schedule.print(std::cout);             break;
				case Command::Group    : schedule.print_group(std::cout);       break;
				case Command::All      : schedule.print_allocations(std::cout); break;
				default       : throw Error{"Unrecognized command!"};
			} break;

			case Command::Add : switch (object) {
				case Command::Person      : schedule.add_person();      break;
				case Command::Room        : schedule.add_room();        break;
				case Command::Meeting     : schedule.add_meeting();     break;
				case Command::Participant : schedule.add_participant(); break;
				default : throw Error{"Unrecognized command!"};
			} break;

			case Command::Reschedule : {
				if (object == Command::Meeting) schedule.reschedule();
				else throw Error{"Unrecognized command!"};
			} break;

			case Command::Del : switch (object) {
				case Command::Person      : schedule.del_person();      break;
				case Command::Room        : schedule.del_room();        break;
				case Command::Meeting     : schedule.del_meeting();     break;
				case Command::Participant : schedule.del_participant(); break;
				case Command::Schedule    : schedule.del();             break;
				case Command::Group       : schedule.del_group();       break;
				case Command::All         : schedule.del_all();         break;
				default : throw Error{"Unrecognized command!"};
			} break;

			case Command::Save : {
				if (object == Command::Data) schedule.save();
				else throw Error{"Unrecognized command!"};
			} break;

			case Command::Load : {
				if (object == Command::Data) schedule.load();
				else throw Error{"Unrecognized command!"};
			} break;

			default : throw Error{"Unrecognized command!"};
			}

		} catch (Error const& e) {
			std::cout << e.what() << '\n';
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}

	std::cout << "Done" << '\n';

} catch (std::exception const& e) {
	std::cerr << "fatal error: " << e.what() << std::endl;
}

std::istream& operator>>(std::istream& is, Command::Action& action) {
	char c; is >> c;
	action = static_cast<Command::Action>(c);
	return is;
}

std::istream& operator>>(std::istream& is, Command::Object& object) {
	char c; is >> c;
	object = static_cast<Command::Object>(c);
	return is;
}
