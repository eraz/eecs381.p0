### General C++ Makefile (Project) written by eraz.
###
### Written to work with most simple C++ starting code without hassle.
### Designed for larger projects that require a directory structure.
### Feel free to copy, modify, or share at will. Credit if you so desire.

EXE     = main
SRC     = cpp
RTA     =

BINDIR  = bin
TARGET  = $(BINDIR)/$(EXE)
BLDDIR  = build
INCDIR  =
LIBDIR  =
SRCDIR  = src

# Compilation settings.
CXX        = g++
CXXVERSION = -std=c++14
CXXFLAGS   = -Wall -Wextra -pedantic
LFLAGS     =
LDFLAGS    =

# Build configurations.
release: CXXFLAGS += -O3 -DNDEBUG
release: all

debug: CXXFLAGS += -g3 -DDEBUG
debug: all

profile: CXXFLAGS += -pg
profile: all

all: $(TARGET)

run: release
	cd $(BINDIR) && ./$(EXE) $(RTA)

check: debug
	cd $(BINDIR) && valgrind -q --leak-check=full ./$(EXE) $(RTA)

clean:
	rm -f $(TARGET) $(BLDDIR)/*

build:
	mkdir -p -- "./bin"
	mkdir -p -- "./build"
	mkdir -p -- "./src"
	printf ".*\n!.gitignore\n*.o\n*.d\n*.exe\ninput*\noutput*\nbuild/*\n" > .gitignore
	printf "*\n!.gitignore\n" > ./bin/.gitignore
	printf "*\n!.gitignore\n" > ./build/.gitignore
	[ -f ./src/main.$(SRC) ] || printf "int main() {}\n" > ./src/main.$(SRC)
	printf "$$SUBMAKE" > ./src/Makefile

# File and dependency parser.
SOURCES := $(wildcard $(SRCDIR)/*.$(SRC))
OBJECTS := $(SOURCES:$(SRCDIR)/%.$(SRC)=$(BLDDIR)/%.o)
DEPS    := $(OBJECTS:$(BLDDIR)/%.o=$(BLDDIR)/%.d)
ifneq ($(MAKECMDGOALS),clean)
sinclude $(DEPS)
endif

# Compilation commands.
$(TARGET): $(OBJECTS)
	$(CXX) $(CXXVERSION) $(CXXFLAGS) -o $(TARGET) $(OBJECTS) $(LDFLAGS) $(LFLAGS)

$(BLDDIR)/%.o: $(SRCDIR)/%.$(SRC) Makefile
	$(CXX) $(CXXVERSION) $(CXXFLAGS) -MMD -o $@ -c $< $(LDFLAGS)

define SUBMAKE
### General C++ Subdirectory Makefile written by eraz.
###
### Written to work in combination with the General C++ Makefile (Project).
### Feel free to copy, modify, or share at will. Credit if you so desire.

DEFAULT:
	@$$(MAKE) -C .. --no-print-directory ||:

$$(MAKECMDGOALS):
	@$$(MAKE) -C .. --no-print-directory $$(MAKECMDGOALS) ||:

# No output files.
.PHONY: DEFAULT $$(MAKECMDGOALS)

# Disable built-in rules.
.SUFFIXES:\n
endef
export SUBMAKE

# No output files.
.PHONY: all release debug profile build clean

# Disable built-in rules.
.SUFFIXES:
